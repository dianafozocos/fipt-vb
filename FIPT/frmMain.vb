﻿Imports System.ComponentModel

Public Class frmMain
    Private ocolOpenForm As New Collection
    Dim WithEvents fOrder As frmOrder

    '--- Open Form
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Call DoLanguageSwitch("English")
        RefreshCapData()

        Dim sSql As String
        Dim oConn As New clsConnADODB(INI_FILE)
        Dim oRs As New clsRsADODB
        Dim rs As New ADODB.Recordset

        sSql = "Select * from Customers"
        Try
            rs = oRs.GetOpenRcs(sSql, oConn.GetConection)
            For i = 0 To rs.RecordCount - 1
                rs.MoveNext()
            Next

            rs.Close()
            rs.ActiveConnection = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    '--- Close form
    Private Sub frmMain_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If MsgBox("Do you want to close " & Me.Text & " ?", vbQuestion + vbYesNo, "Confirm exit") = vbYes Then
            'If MsgBox(sGetStringLiteral(12) & " " & Me.Text & "?", vbQuestion + vbYesNo, sGetStringLiteral(13)) = vbYes Then
            '--- closing all sub-windows
            For Each f In ocolOpenForm
                f.Dispose()
                f = Nothing
            Next
            e.Cancel = 0
        Else
            e.Cancel = 1
        End If
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    '--- Translate in English
    Private Sub EnglishToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnglishToolStripMenuItem.Click
        Call DoLanguageSwitch("English")
        RefreshCapData()
    End Sub

    '--- Translate in French
    Private Sub FrenchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FrenchToolStripMenuItem.Click
        Call DoLanguageSwitch("French")
        RefreshCapData()
    End Sub

    '--- Refresh Captions
    Public Sub RefreshCapData()
        FrenchToolStripMenuItem.Text = sGetStringLiteral(5)
        EnglishToolStripMenuItem.Text = sGetStringLiteral(6)
        tsLanguage.Text = sGetStringLiteral(7)
    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        If fOrder Is Nothing Then
            fOrder = New frmOrder
            With fOrder
                .Show()
                .InitForm()
            End With
        Else
            fOrder.Activate()
        End If
    End Sub
    '--- Closes Search Slot form
    Private Sub ffOrder_CloseForm() Handles fOrder.CloseForm
        fOrder.Dispose()
        fOrder = Nothing
    End Sub

End Class
