﻿Option Explicit On
Option Strict Off
Imports Microsoft.VisualBasic
Imports System.Data.OleDb


Module ModuleApp

    ''' <summary>
    ''' SQL server connexion parameters
    ''' </summary>
#If DEBUG Then

#Else
    Public INI_FILE As String = My.Application.Info.DirectoryPath & "\FIPT.ini"
#End If


    Public User As String = Security.Principal.WindowsIdentity.GetCurrent().Name
    Public sElec, sMeca As Boolean
    Public bAdmin As Boolean = False

    'Load data into Recordset and grid
    Public Sub InitData(ByVal sSql As String, ByRef rs As ADODB.Recordset, ByRef tdbgData As DataGrid)
        Dim oRsADODB As New clsRsADODB
        Dim oConn As New clsConnADODB(INI_FILE)

        Try
            rs = oRsADODB.GetOpenRcs(sSql, oConn.GetConection)
            With tdbgData
                .DataSource = rs
                .Refresh()
            End With
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try
    End Sub
    'load a combo list  from a  recordset
    Public Sub Load1List(ByRef cboList As ComboBox, ByVal sSql As String)
        Dim oConn As New clsConnADODB(INI_FILE)
        Dim oRsADODB As New clsRsADODB
        Dim rs As New ADODB.Recordset
        rs = oRsADODB.GetOpenRcs(sSql, oConn.GetConection())
        With rs
            If (rs.RecordCount > 0) Then
                cboList.Items.Clear()
                Do Until .EOF
                    cboList.Items.Add(Trim(.Fields(0).Value))
                    .MoveNext()
                Loop
                cboList.SelectedIndex = 0
            End If
            .Close()
        End With
        rs = Nothing
        oRsADODB = Nothing
        oConn = Nothing
    End Sub

    Public Function RecordSetToDataTable(ByVal objRS As ADODB.Recordset) As DataTable
        Dim objDA As New OleDbDataAdapter()
        Dim objDT As New DataTable()
        objDA.Fill(objDT, objRS)
        Return objDT
    End Function
    Public Function GetNotNullDateTime(ByVal field As ADODB.Field) As String
        Dim objdate As DateTime

        If field.Value Is System.DBNull.Value Then
            Return ""
        Else
            objdate = Date.Parse(field.Value)
            Return objdate.ToString("MM/dd/yyyy")
        End If
    End Function
    'Declare Operation modes
    Public Enum teOperation
        enDisplay
        enAdd
        enEdit
        enDelete
        enError
    End Enum
    'Display status info in status bar and data panel in operation specific color
    Public Sub DisplayFrmStatus(ByVal enCurretStatus As teOperation, ByRef tslFormStatus As ToolStripLabel)
        Select Case enCurretStatus
            Case teOperation.enDisplay
                With tslFormStatus
                    .Text = "DISPLAY"
                    .BackColor = DISPLAY_COLOR
                End With
            Case teOperation.enError
                With tslFormStatus
                    .Text = "ERROR"
                    .BackColor = ERROR_COLOR
                End With
            Case teOperation.enAdd
                With tslFormStatus
                    .Text = "ADD"
                    .BackColor = ADD_COLOR
                End With
            Case teOperation.enEdit
                With tslFormStatus
                    .Text = "EDIT"
                    .BackColor = EDIT_COLOR
                End With

            Case teOperation.enDelete
                With tslFormStatus
                    .Text = "DELETE"
                    .BackColor = DELETE_COLOR
                End With
        End Select
    End Sub
    Public DISPLAY_COLOR As System.Drawing.Color = System.Drawing.Color.SeaGreen
    Public ADD_COLOR As System.Drawing.Color = System.Drawing.Color.DarkSeaGreen
    Public EDIT_COLOR As System.Drawing.Color = System.Drawing.Color.DarkSeaGreen
    Public DELETE_COLOR As System.Drawing.Color = System.Drawing.Color.DarkKhaki
    Public ERROR_COLOR As System.Drawing.Color = System.Drawing.Color.IndianRed
End Module

