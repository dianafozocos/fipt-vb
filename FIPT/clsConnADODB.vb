﻿Option Strict Off
Option Explicit On

Public Class clsConnADODB
    'the connection string
    Private gsDBConnString As String

    Sub New(ByVal sIniFile As String)
        'the database name
        Dim gsDBName As String
        'the database server
        Dim gsDBServer As String
        'the user id to conect to database
        Dim gsDBUserID As String
        'the password to conect to database
        Dim gsDBPass As String
        'the path to ini file
        Dim sPathToIniFile As String = sIniFile

        'read the connection string arguments from the SQUALS.ini
        gsDBName = sGetPrivProfileString("Environment", "DATABASE", sPathToIniFile)
        gsDBServer = sGetPrivProfileString("Environment", "SERVER", sPathToIniFile)
        gsDBUserID = sGetPrivProfileString("Environment", "UID", sPathToIniFile) ' & User
        gsDBPass = sGetPrivProfileString("Environment", "PSW", sPathToIniFile)

        gsDBConnString = "DRIVER={SQL Server}; SERVER=" & gsDBServer & "; DATABASE=" & gsDBName & "; UID=" & gsDBUserID & "; PWD=" & gsDBPass

    End Sub

    Function GetConection() As ADODB.Connection
        'the conection obiect
        Dim oConn As New ADODB.Connection
        Try
            With oConn
                'initializare parametrii
                .Attributes = 0
                .CommandTimeout = 100
                .ConnectionString = gsDBConnString
                .ConnectionTimeout = 15
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .IsolationLevel = ADODB.IsolationLevelEnum.adXactCursorStability
                .Mode = ADODB.ConnectModeEnum.adModeUnknown
                .Open()
            End With
            GetConection = oConn
        Catch e As Exception
            GetConection = Nothing
            MsgBox("GetConection: " & e.Message)
        End Try
    End Function
End Class
