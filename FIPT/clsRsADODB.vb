﻿Public Class clsRsADODB

    'open recordset
    Function GetOpenRcs(ByVal sSQlSelect As String, ByVal oConn As ADODB.Connection) As ADODB.Recordset
        Dim rs As New ADODB.Recordset
        Try
            With rs
                .ActiveConnection = oConn
                .CursorLocation = ADODB.CursorLocationEnum.adUseClient
                .CursorType = ADODB.CursorTypeEnum.adOpenKeyset
                .LockType = ADODB.LockTypeEnum.adLockOptimistic
                .Open(sSQlSelect)
            End With
            GetOpenRcs = rs
        Catch ex As Exception
            GetOpenRcs = Nothing
            Throw New ApplicationException("Open rcs: " & sSQlSelect & " " & ex.Message)
        End Try
    End Function

End Class
