﻿Imports System.Threading
Imports System.Globalization
Imports System.ComponentModel

Public Class frmOrder

    'event raised when form get focus
    Public Event FocusForm()
    'event raised when form close
    Public Event CloseForm()
    'Specify the current status of the form
    Private enCurrentStatus As teOperation
    Dim dtDocs As New DataTable


    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        enCurrentStatus = teOperation.enDisplay
        DisplayFrmStatus(enCurrentStatus, tslFormStatus)
        tdgDocs.Enabled = True
    End Sub

    Private Sub frmOrder_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If MsgBox("Close window " & Me.Text & " ?", vbQuestion + vbYesNo) = vbYes Then
            RaiseEvent CloseForm()
            e.Cancel = 0
        Else
            e.Cancel = 1
        End If
    End Sub

    Private Sub frmOrder_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        RaiseEvent FocusForm()
    End Sub

    '--- Init Form
    Public Sub InitForm()
        enCurrentStatus = teOperation.enDisplay
        DisplayFrmStatus(enCurrentStatus, tslFormStatus)

        InitGrid
        LoadGrid

        RefreshCapData()
    End Sub

    'Init Grid
    Private Sub InitGrid()
        With dtDocs
            .Columns.Clear()
            .Columns.Add("name", GetType(String))
            .Columns.Add("longDesc", GetType(String))
            .Columns.Add("revision", GetType(Integer))
            .Columns.Add("outsideUrl", GetType(String))
        End With

        tdgDocs.DataSource = dtDocs
        tdgDocs.Columns("name").HeaderText = "Name"
        tdgDocs.Columns("longDesc").HeaderText = "Description"
        tdgDocs.Columns("revision").HeaderText = "Revision"
        tdgDocs.Columns("outsideUrl").HeaderText = "Path"

        tdgDocs.Columns("name").Width = 190
        tdgDocs.Columns("longDesc").Width = 200
        tdgDocs.Columns("revision").Width = 50
        tdgDocs.Columns("outsideUrl").Width = 400
    End Sub
    'Load Grig with record from database
    Private Sub LoadGrid()
        Dim oConn As New clsConnADODB(INI_FILE)
        Dim oRs As New clsRsADODB
        Dim rs As New ADODB.Recordset
        Dim sSql As String
        Dim dr As DataRow

        Try
            sSql = "Select * from Documents where workOrder ='" & frmMain.txtOrder.Text & "'"
            rs = oRs.GetOpenRcs(ssql, oConn.GetConection)
            dtDocs.Rows.Clear()
            For i = 0 To rs.RecordCount - 1
                dr = dtDocs.NewRow
                dr("Name") = GetNotNullValue(rs.Fields("Name"))
                dr("longDesc") = GetNotNullValue(rs.Fields("longDesc"))
                dr("Revision") = GetNotNullValue(rs.Fields("Revision"))
                dr("outsideUrl") = GetNotNullValue(rs.Fields("outsideUrl"))
                dtDocs.Rows.Add(dr)
                rs.MoveNext()
            Next
            rs.Close()
        Catch ex As Exception
            MsgBox("Error on loading users" & ex.Message)
            'MsgBox(sGetStringLiteral(25) & ex.Message)
        End Try
    End Sub

    Public Sub RefreshCapData()
        ' btnNew.Text = sGetStringLiteral(20)
        ' btnCancel.Text = sGetStringLiteral(21)
    End Sub

End Class