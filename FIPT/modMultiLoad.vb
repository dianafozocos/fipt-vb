Option Strict Off
Option Explicit On
Module modMultiLoad

    Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer

    Public gsLanguage As String
    Public gbIsRomana As Boolean

    Public Function sGetResourceString(ByVal sAppName As String, ByVal sKeyName As String, ByVal sININame As String) As String

        'Calls the windows API to get a value from a profile string entry

        Dim sAnswer As String
        Dim iSize As Object
        Dim iValid As Short
        Dim lpReturnString As String

        'Setup the local error handler...
        On Error GoTo ehGRS

        lpReturnString = Space(180)
        iSize = Len(lpReturnString)

        iValid = GetPrivateProfileString(sAppName, sKeyName, "", lpReturnString, iSize, sININame)

        '// Discard the trailing spaces and null character.
        sAnswer = Left(lpReturnString, iValid)

ehGRSExit:
        sGetResourceString = sAnswer
        Exit Function

        'Local error handler.
ehGRS:
        'Call DisplayErr(Err.Number, Err.Description & " (GetResourceString):)" & Err.Source)
        Resume ehGRSExit

    End Function

    '##ModelId=4BAC7DE700B3
    Public Function sGetStringLiteral(ByRef lStrNum As Integer) As String

        'Required function to repopulate form captions and text boxes with...
        'the String Literals that have been stripped out during the parsing process.

        Dim sKeyName As String
        Dim sININame As String
        Dim sLang As String
        Dim sRet As String

        'Setup the local error handler...
        On Error GoTo ehGSL

        'Get the language we are using...
        sLang = gsLanguage
        'Set the file name...
        sININame = My.Application.Info.DirectoryPath & "\Language.ini"
        'Convert the key name to a string...
        sKeyName = Trim(CStr(lStrNum))

        'Attempt to get the requested information...
        sRet = sGetResourceString(sLang, sKeyName, sININame)

ehGSLExit:
        'Return the value...
        sGetStringLiteral = sRet

        Exit Function

        'Local error handler.
ehGSL:
        'Call DisplayErr(Err.Number, Err.Description & " (GetStringLiteral):)" & Err.Source)
        Resume ehGSLExit

    End Function

    '	'##ModelId=4BAC7DE700B5
    Public Sub DoLanguageSwitch(ByRef sNewLang As String)

        'Resets the current language in the in the resource file and sets the...
        'global language variable 'gsLanguage' to the value specified in this call.

        Dim lX As Integer
        Dim sTmp As String
        Dim sININame As String

        'Setup the local error handler...
        On Error GoTo ehDLS

        'Set the resource file path and name...
        sININame = My.Application.Info.DirectoryPath & "\Language.ini"

        'Get a copy of the current language...
        sTmp = gsLanguage
        'If the requested language is different from the current one, switch...
        If UCase(sTmp) <> UCase(sNewLang) Then
            'Write the new value to the ini file...
            lX = WritePrivateProfileString("General", "CurrentLangage", sNewLang, sININame)
            'Reset the global language variable...
            gsLanguage = sNewLang
        End If

ehDLSExit:
        Exit Sub

        'Local error handler.
ehDLS:
        'Call DisplayErr(Err.Number, Err.Description & " (DoLanguageSwitch):)" & Err.Source)
        Resume ehDLSExit

    End Sub

    '##ModelId=4BAC7DE700C0
    Public Function sGetCurrentLanguage() As String

        'Reads the resource file to see what the current language setting is. Returns...
        'the information to the calling proc. This call needs to be made initially on...
        'program load so the Caller can set the global gsLanguage variable based upon...
        'the value returned by this proc.

        Dim sAnswer As String
        Dim iSize As Object
        Dim iValid As Short
        Dim lpReturnString As String
        Dim sININame As String

        'Setup the local error handler...
        On Error GoTo ehGCL

        sININame = My.Application.Info.DirectoryPath & "\Language.ini"

        lpReturnString = Space(180)
        'UPGRADE_WARNING: Couldn't resolve default property of object iSize. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        iSize = Len(lpReturnString)

        'UPGRADE_WARNING: Couldn't resolve default property of object iSize. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        iValid = GetPrivateProfileString("General", "CurrentLangage", "", lpReturnString, iSize, sININame)

        'Discard the trailing spaces and null character.
        sAnswer = Left(lpReturnString, iValid)

ehGCLExit:
        'Set the return value...
        sGetCurrentLanguage = sAnswer

        Exit Function

        'Local error handler.
ehGCL:
        'Call DisplayErr(Err.Number, Err.Description & " (GetCurrentLanguage):)" & Err.Source)
        Resume ehGCLExit

    End Function

    'Calls the windows API to get a value from a profile string entry
    Public Function sGetPrivProfileString(ByVal sAppName As String, ByVal sKeyName As String, ByVal sININame As String) As String
        Dim sAnswer As String
        Dim iSize As Integer
        Dim iValid As Short
        Dim lpReturnString As String

        lpReturnString = Space(128)
        iSize = Len(lpReturnString)

        iValid = GetPrivateProfileString(sAppName, sKeyName, "", lpReturnString, iSize, sININame)

        '// Discard the trailing spaces and null character.
        sAnswer = Left(lpReturnString, iValid)
        sGetPrivProfileString = sAnswer

    End Function

    'Enclose a string in single quotation marks.
    Public Function sSglQt(ByVal sString As String) As String
        Dim sTmp As String
        Dim iI As Short
        Dim sChar As String

        sTmp = ""
        For iI = 1 To Len(sString)
            sChar = Mid(sString, iI, 1)
            If sChar = "'" Then
                sTmp = sTmp & sChar & "'"
            Else
                sTmp = sTmp & sChar
            End If '// sChar is a single quote
        Next iI

        sString = sTmp
        sSglQt = "'" & sString & "'"
    End Function

    Public Function GetNotZeroValue(ByVal field As ADODB.Field) As Decimal
        If field.Value Is System.DBNull.Value Then
            Return 0
        Else
            Return field.Value
        End If
    End Function

    Public Function GetNotNullValue(ByVal field As ADODB.Field) As String
        If field.Value Is System.DBNull.Value Then
            Return ""
        Else
            Return field.Value
        End If
    End Function

    Public Function GetNotNullDate(ByVal field As ADODB.Field) As String
        Dim objdate As Date

        If field.Value Is System.DBNull.Value Then
            Return ""
        Else
            objdate = Date.Parse(field.Value)
            Return objdate.ToString("MM/dd/yyyy")
        End If
    End Function
End Module