﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.StatusStripMain = New System.Windows.Forms.StatusStrip()
        Me.tsLanguage = New System.Windows.Forms.ToolStripSplitButton()
        Me.EnglishToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FrenchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.txtOrder = New System.Windows.Forms.TextBox()
        Me.txtPhase = New System.Windows.Forms.TextBox()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.lblOrder = New System.Windows.Forms.Label()
        Me.lblPhase = New System.Windows.Forms.Label()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.StatusStripMain.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStripMain
        '
        Me.StatusStripMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsLanguage})
        Me.StatusStripMain.Location = New System.Drawing.Point(0, 185)
        Me.StatusStripMain.Name = "StatusStripMain"
        Me.StatusStripMain.Size = New System.Drawing.Size(420, 22)
        Me.StatusStripMain.TabIndex = 8
        Me.StatusStripMain.Text = "StatusStripMain"
        '
        'tsLanguage
        '
        Me.tsLanguage.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EnglishToolStripMenuItem, Me.FrenchToolStripMenuItem})
        Me.tsLanguage.Image = CType(resources.GetObject("tsLanguage.Image"), System.Drawing.Image)
        Me.tsLanguage.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsLanguage.Name = "tsLanguage"
        Me.tsLanguage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tsLanguage.Size = New System.Drawing.Size(91, 20)
        Me.tsLanguage.Text = "Language"
        Me.tsLanguage.ToolTipText = "Language"
        '
        'EnglishToolStripMenuItem
        '
        Me.EnglishToolStripMenuItem.Image = CType(resources.GetObject("EnglishToolStripMenuItem.Image"), System.Drawing.Image)
        Me.EnglishToolStripMenuItem.Name = "EnglishToolStripMenuItem"
        Me.EnglishToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.EnglishToolStripMenuItem.Text = "English"
        '
        'FrenchToolStripMenuItem
        '
        Me.FrenchToolStripMenuItem.Image = CType(resources.GetObject("FrenchToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FrenchToolStripMenuItem.Name = "FrenchToolStripMenuItem"
        Me.FrenchToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.FrenchToolStripMenuItem.Text = "French"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.PictureBox1.Location = New System.Drawing.Point(0, 1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(419, 35)
        Me.PictureBox1.TabIndex = 30
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(311, 1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(97, 35)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 31
        Me.PictureBox2.TabStop = False
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(151, 59)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(180, 20)
        Me.txtUser.TabIndex = 32
        '
        'txtOrder
        '
        Me.txtOrder.Location = New System.Drawing.Point(151, 87)
        Me.txtOrder.Name = "txtOrder"
        Me.txtOrder.Size = New System.Drawing.Size(180, 20)
        Me.txtOrder.TabIndex = 33
        '
        'txtPhase
        '
        Me.txtPhase.Location = New System.Drawing.Point(151, 115)
        Me.txtPhase.Name = "txtPhase"
        Me.txtPhase.ReadOnly = True
        Me.txtPhase.Size = New System.Drawing.Size(180, 20)
        Me.txtPhase.TabIndex = 34
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Location = New System.Drawing.Point(92, 63)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(51, 13)
        Me.lblUser.TabIndex = 35
        Me.lblUser.Text = "USER ID"
        '
        'lblOrder
        '
        Me.lblOrder.AutoSize = True
        Me.lblOrder.Location = New System.Drawing.Point(62, 91)
        Me.lblOrder.Name = "lblOrder"
        Me.lblOrder.Size = New System.Drawing.Size(83, 13)
        Me.lblOrder.TabIndex = 36
        Me.lblOrder.Text = "SALES ORDER"
        '
        'lblPhase
        '
        Me.lblPhase.AutoSize = True
        Me.lblPhase.Location = New System.Drawing.Point(25, 119)
        Me.lblPhase.Name = "lblPhase"
        Me.lblPhase.Size = New System.Drawing.Size(118, 13)
        Me.lblPhase.TabIndex = 37
        Me.lblPhase.Text = "PRODUCTION PHASE"
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(260, 141)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(71, 30)
        Me.btnOk.TabIndex = 38
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(420, 207)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.lblPhase)
        Me.Controls.Add(Me.lblOrder)
        Me.Controls.Add(Me.lblUser)
        Me.Controls.Add(Me.txtPhase)
        Me.Controls.Add(Me.txtOrder)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.StatusStripMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Login"
        Me.StatusStripMain.ResumeLayout(False)
        Me.StatusStripMain.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents StatusStripMain As StatusStrip
    Friend WithEvents tsLanguage As ToolStripSplitButton
    Friend WithEvents EnglishToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FrenchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents txtUser As TextBox
    Friend WithEvents txtOrder As TextBox
    Friend WithEvents txtPhase As TextBox
    Friend WithEvents lblUser As Label
    Friend WithEvents lblOrder As Label
    Friend WithEvents lblPhase As Label
    Friend WithEvents btnOk As Button
End Class
